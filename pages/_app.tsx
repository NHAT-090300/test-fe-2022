import React, { ReactElement } from 'react'
import { wrapper } from 'app/store'
import 'bootstrap/dist/css/bootstrap.min.css'

function ETypingApp({ Component, pageProps: { ...pageProps } }: any) {
  const getLayout = Component.getLayout || ((page: ReactElement) => page)
  return getLayout(
    <>
      <Component {...pageProps} />
    </>
  )
}

export default wrapper.withRedux(ETypingApp)
