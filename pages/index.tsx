import { ReactElement } from 'react'
import MainLayout from 'layout/MainLayout'
import Index from './blogs'

Index.getLayout = function getLayout(page: ReactElement) {
  return <MainLayout title='TEST | Blogs'>{page}</MainLayout>
}
export default Index
