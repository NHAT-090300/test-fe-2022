import { ReactElement, Dispatch, useEffect } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { getDetailBlog, IBlogState, updateBlog } from 'features/blog/blogSlice'
// ui
import { useRouter } from 'next/router'
import MainLayout from 'layout/MainLayout'
import BlogCreateForm from 'features/blog/createFrom/BlogCreateForm'

function Update({ blog, handleDispatch, handleDispatchUpdate }: any) {
  const router = useRouter()
  const { blogId } = router.query

  useEffect(() => {
    blogId && handleDispatch(blogId)
  }, [blogId])

  return (
    <>
      <div className='container'>
        <BlogCreateForm dispatch={handleDispatchUpdate} initValue={blog} />
      </div>
    </>
  )
}

const mapStateToProps = ({ blog }: { blog: IBlogState }) => ({
  blog: blog.getDetailBlog.detail,
})

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
  handleDispatch: (id: number) => dispatch(getDetailBlog(id)),
  handleDispatchUpdate: (params: any) => dispatch(updateBlog(params)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

Update.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainLayout title='TEST | Blog Detail' name='Edit blog'>
      {page}
    </MainLayout>
  )
}
export default compose(withConnect)(Update)
