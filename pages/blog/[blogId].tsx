import { ReactElement, Dispatch, useEffect } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { getDetailBlog, IBlogState } from 'features/blog/blogSlice'
// ui
import { useRouter } from 'next/router'
import MainLayout from 'layout/MainLayout'
import BlogDetail from 'features/blog/blogDetail/BlogDetail'

function Detail({ blog, handleDispatch }: any) {
  const router = useRouter()
  const { blogId } = router.query

  useEffect(() => {
    blogId && handleDispatch(blogId)
  }, [blogId])

  return (
    <>
      <div className='container'>
        <div className='row'>
          <BlogDetail
            id={blogId}
            title={blog?.title}
            image={blog?.image?.url}
            content={blog?.content}
            createAt={blog?.updated_at}
          />
        </div>
      </div>
    </>
  )
}

const mapStateToProps = ({ blog }: { blog: IBlogState }) => ({
  blog: blog.getDetailBlog.detail,
})

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
  handleDispatch: (id: number) => dispatch(getDetailBlog(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

Detail.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainLayout title='TEST | Blog Detail' name='Blog'>
      {page}
    </MainLayout>
  )
}
export default compose(withConnect)(Detail)
