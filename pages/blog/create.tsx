import { ReactElement, Dispatch } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import BlogCreateForm from 'features/blog/createFrom/BlogCreateForm'
import MainLayout from 'layout/MainLayout'
import { createBlog, IBlogState } from 'features/blog/blogSlice'
import { IBlogCreateForm } from 'api/blog'

const Create = ({ listBlog, handleDispatch }: any) => {
  console.log(listBlog)
  return (
    <>
      <div className='container'>
        <BlogCreateForm dispatch={handleDispatch} />
      </div>
    </>
  )
}

const mapStateToProps = ({ blog }: { blog: IBlogState }) => ({
  listBlog: blog.createBlog,
})

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
  handleDispatch: (params: IBlogCreateForm) => dispatch(createBlog(params)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

Create.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainLayout title='TEST | Blog Detail' name='Make blog'>
      {page}
    </MainLayout>
  )
}

export default compose(withConnect)(Create)
