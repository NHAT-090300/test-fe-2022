import { useEffect, ReactElement, Dispatch, useState } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { getListBlog, IBlogState, deleteBlog } from 'features/blog/blogSlice'
// ui
import MainLayout from 'layout/MainLayout'
import { IQuery } from 'types/query'
import { IBlog } from 'api/blog'
import SearchFeild from 'ui-component/Feild/SearchFeild'
import Pagination from 'ui-component/Pagination'
import useDebounce from 'hooks/useDebounce'
import Card from 'ui-component/Card'

const Blogs = ({ listBlog, handleDispatch, handleDispatchDelete }: any) => {
  const [textSearch, setTextSearch] = useState('')
  const [page, setPage] = useState(1)
  const debouncedValue = useDebounce<string>(textSearch, 500)

  useEffect(() => {
    handleDispatch({
      offset: 10,
      search: textSearch,
      page: page,
    })
  }, [debouncedValue, page])

  const handleDelete = (id: number) => {
    handleDispatchDelete(id)
  }

  return (
    <>
      <div className='container'>
        <div className='row'>
          <div className='col-md-8 text-start my-5 '>
            <h3 className='z-index-1 position-relative'>List blog</h3>
            <p className='opacity-8 mb-0'>
              There’s nothing I really wanted to do in life that I wasn’t able
              to get good at. That’s my skill.
            </p>
          </div>
        </div>
        <div className='row'>
          <div className='col-lg-5 text-end z-index-2 border-radius-xl my-3'>
            <div className='row'>
              <div className='col-md-12'>
                <SearchFeild
                  getValue={(value: string) => {
                    setTextSearch(value)
                  }}
                />
              </div>
            </div>
          </div>
        </div>
        <div className='row'>
          {listBlog.result &&
            listBlog.result.map((blog: IBlog, i: number) => (
              <Card
                key={i}
                id={blog.id}
                image={blog?.image?.url}
                title={blog.title}
                creatAt={blog.created_at}
                handleDelete={handleDelete}
              />
            ))}
        </div>
        <div className='row d-flex justify-content-center'>
          <Pagination
            total={listBlog.paginate?.total}
            page={listBlog.paginate?.page}
            getPaginate={(number) => {
              setPage(number)
            }}
            next={listBlog.paginate?.next}
            prev={listBlog.paginate?.prev}
          />
        </div>
      </div>
    </>
  )
}
const mapStateToProps = ({ blog }: { blog: IBlogState }) => ({
  listBlog: blog.getListBlog.list,
})

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
  handleDispatch: (params: IQuery) => dispatch(getListBlog(params)),
  handleDispatchDelete: (id: number) => dispatch(deleteBlog(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

Blogs.getLayout = function getLayout(page: ReactElement) {
  return <MainLayout title='DaPass | List Blog'>{page}</MainLayout>
}
export default compose(withConnect)(Blogs)
