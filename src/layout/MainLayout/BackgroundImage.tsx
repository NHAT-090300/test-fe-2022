import React from 'react'
import style from './mainLayout.module.scss'

const BackgroundImage = ({ src = '/images/curved8.jpg' }: { src?: string }) => {
  return (
    <>
      <img src={src} alt='' className={style.content_bg} />
    </>
  )
}

export default BackgroundImage
