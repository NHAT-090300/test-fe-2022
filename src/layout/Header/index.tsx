import React from 'react'
import style from './header.module.scss'
import { useRouter } from 'next/router'
import Button from './Button'

const header = [
  {
    name: 'Blogs',
    url: '/blogs',
  },
  {
    name: 'Make blog',
    url: '/blog/create',
  },
  {
    name: 'Contact',
    url: '',
  },
]

const Header = () => {
  const router = useRouter()

  return (
    <div className='container-fluid p-0 position-sticky z-index-sticky top-2 d-flex justify-content-center my-2'>
      <nav className='navbar opacity-25 navbar-expand-lg navbar-light bg-light col-9 rounded-pill px-4'>
        <span className='mr-3'>
          <img src='/logo-social.png' alt='' className={style.nav_logo} />
        </span>
        <button
          className='navbar-toggler'
          type='button'
          data-toggle='collapse'
          data-target='#navbarSupportedContent'
          aria-controls='navbarSupportedContent'
          aria-expanded='false'
          aria-label='Toggle navigation'
        >
          <span className='navbar-toggler-icon' />
        </button>
        <div className='collapse navbar-collapse' id='navbarSupportedContent'>
          <ul className='navbar-nav mr-auto w-50 d-flex justify-content-around m-auto'>
            {header.map((item, i) => (
              <li
                key={i}
                onClick={() => item.url && router.push(item.url)}
                className={`nav-item active ${style.nav_link} ${
                  router.route === item.url ? style.nav_link_active : ''
                }`}
              >
                <a>
                  {item.name} <span className='sr-only'>(current)</span>
                </a>
              </li>
            ))}
          </ul>
          <Button>Login</Button>
        </div>
      </nav>
    </div>
  )
}

export default Header
