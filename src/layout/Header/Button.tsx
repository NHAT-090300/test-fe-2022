import React from 'react'
import style from './header.module.scss'

function Button({ children }: any) {
  return (
    <button type='button' className={style.nav_btn}>
      {children}
    </button>
  )
}

export default Button
