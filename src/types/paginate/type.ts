interface IPaginate {
  total?: number
  count?: number
  page?: number
  offset?: number
  prev?: number | null
  next?: number | null
}

export default IPaginate
