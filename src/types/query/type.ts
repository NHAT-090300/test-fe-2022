interface IQuery {
  page?: number
  offset?: number
  query?: string
  search?: string
}

export default IQuery
