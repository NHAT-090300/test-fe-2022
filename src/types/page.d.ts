import { NextPage } from 'next'
import { ReactElement, ReactNode } from 'react'

export type Page<P = Record<string, nerver>> = NextPage<P> & {
  getLayout?: (page: ReactElement) => ReactNode
}
