export type IStatus = 'idle' | 'pending' | 'fullfiledd' | 'rejected'

export type IResponse = {
  status: IStatus
  error?: string
}
