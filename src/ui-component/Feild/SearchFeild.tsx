import React, { useState } from 'react'
import { IconSearch } from '@tabler/icons'

const SearchFeild = ({ getValue }: { getValue: (value: string) => void }) => {
  const [value, setValue] = useState('')

  const handleOnChange = (e: any) => {
    setValue(e.target.value)
    getValue(e.target.value)
  }

  return (
    <div>
      <div className={`p-1 rounded rounded-pill bg-white shadow-lg mb-4 `}>
        <div className='input-group'>
          <input
            type='search'
            placeholder='Search'
            className='form-control border-0 bg-white shadow-none mx-3'
            value={value}
            onChange={handleOnChange}
          />
          <div className='input-group-append'>
            <button type='submit' className='btn btn-link text-primary'>
              <IconSearch />
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SearchFeild
