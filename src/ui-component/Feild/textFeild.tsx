import React from 'react'
import styles from './feild.module.scss'
// import { ErrorMessage } from 'formik'
import 'react-quill/dist/quill.snow.css'
import dynamic from 'next/dynamic'

const ReactQuill = dynamic(() => import('react-quill'), { ssr: false })

const TextField = ({
  label,
  type,
  name,
  value,
  handleChange,
  error,
}: {
  label: string
  type?: string
  name: string
  value: string
  error?: any
  handleChange: (value: string) => void
}) => {
  return (
    <div className='mb-4'>
      <label htmlFor={name}>{label}</label>
      {type === 'text' ? (
        <input
          type='text'
          name='name'
          style={{ background: 'none' }}
          className={`form-control shadow-none ${
            Boolean(error) && 'is-invalid'
          }`}
          autoComplete='off'
          value={value}
          onChange={(e) => handleChange(e.target.value)}
        />
      ) : (
        <ReactQuill
          value={value}
          onChange={(value) => {
            if (value === '<p><br></p>') value = ''
            handleChange(value)
          }}
          theme='snow'
          style={{ height: '300px' }}
        />
      )}

      {error && <span className={styles.error}>{error}</span>}
    </div>
  )
}
export default TextField
