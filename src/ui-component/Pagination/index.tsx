import React, { useState } from 'react'
import style from './pagination.module.scss'
function Pagination({
  total,
  prev,
  page,
  next,
  getPaginate,
}: {
  total: number
  page: number
  prev?: number | null
  next?: number | null
  getPaginate: (pageNumber: number) => void
}) {
  const [currentPage, setCurrentPage] = useState(page)
  const pageNumbers = []

  for (let i = 1; i <= total; i++) {
    pageNumbers.push(i)
  }

  const handlePagination = (array: number[]) => {
    if (page < 3) {
      return array.slice(0, 6)
    } else {
      return array.slice(page - 3, page + 2)
    }
  }

  return (
    <nav className='mt-4 mb-5'>
      <ul className={`pagination ${style.pagination_wrap}`}>
        {prev && pageNumbers.length > 1 && (
          <>
            <li
              className='page-item'
              onClick={() => {
                setCurrentPage(page - 1)
                getPaginate(prev)
              }}
            >
              <a className='page-link' aria-label='Previous'>
                <span aria-hidden='true'>prev</span>
              </a>
            </li>
            {handlePagination(pageNumbers)[0] !== 1 && (
              <li
                className='page-item'
                onClick={() => {
                  setCurrentPage(page + 1)
                  getPaginate(prev)
                }}
              >
                <a className='page-link' aria-label='Previous'>
                  <span aria-hidden='true'>...</span>
                </a>
              </li>
            )}
          </>
        )}
        {pageNumbers.length > 1 &&
          handlePagination(pageNumbers).map((pageNumber) => (
            <li
              key={pageNumber}
              className={`page-item ${
                pageNumber === currentPage ? style.pagination_active : ''
              }`}
              onClick={() => {
                setCurrentPage(pageNumber)
                getPaginate(pageNumber)
              }}
            >
              <a className='page-link' aria-label='Previous'>
                <span aria-hidden='true'>{pageNumber}</span>
              </a>
            </li>
          ))}
        {next && pageNumbers.length > 1 && (
          <>
            {total > handlePagination(pageNumbers).slice(-1)[0] && (
              <li
                className='page-item'
                onClick={() => {
                  setCurrentPage(page + 1)
                  getPaginate(next)
                }}
              >
                <a className='page-link' aria-label='Previous'>
                  <span aria-hidden='true'>...</span>
                </a>
              </li>
            )}
            <li
              className='page-item'
              onClick={() => {
                setCurrentPage(page + 1)
                getPaginate(next)
              }}
            >
              <a className='page-link' aria-label='Previous'>
                <span aria-hidden='true'>next</span>
              </a>
            </li>
          </>
        )}
      </ul>
    </nav>
  )
}

export default Pagination
