import React, { useCallback, useEffect, useState } from 'react'
import { useDropzone } from 'react-dropzone'
import { IconUpload } from '@tabler/icons'
import { imageType } from 'utils/constaint'
import style from './dropzone.module.scss'

const Dropzone: React.FC<any> = ({ onFileUploaded, formik }) => {
  const [selectedFileUrl, setSelectedFileUrl] = useState('')

  useEffect(() => {
    const url = formik.values['image']
    !selectedFileUrl && setSelectedFileUrl(url)
  }, [formik])
  const onDrop = useCallback(
    (acceptedFiles: any) => {
      const file = acceptedFiles[0]
      const fileUrl = URL.createObjectURL(file)
      setSelectedFileUrl(fileUrl)
      onFileUploaded(file)
    },
    [onFileUploaded]
  )

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: imageType,
  })

  return (
    <div className={style.dropzone} {...getRootProps()}>
      <input {...getInputProps()} accept={`${imageType}`} />
      {selectedFileUrl ? (
        <img src={selectedFileUrl} alt='' />
      ) : (
        <p className='m-0'>
          <IconUpload />
        </p>
      )}
      {formik.errors['image'] && (
        <span className={style.error}>{formik.errors['image']}</span>
      )}
    </div>
  )
}

export default Dropzone
