/* eslint-disable react/no-children-prop */
import React from 'react'
import style from './card.module.scss'
import { useRouter } from 'next/router'
import ChipDate from './Chip'
import Dropdown from './Dropdown'
import { IconSquareX } from '@tabler/icons'

interface CardProps {
  id: number
  image: string
  title: string
  creatAt: string
  handleDelete: (id: number) => void
}
const Card = ({ image, title, creatAt, id, handleDelete }: CardProps) => {
  const router = useRouter()
  return (
    <div className={`col-lg-6 col-12 mb-4 ${style.card_blog}`}>
      <Dropdown />
      <div
        className={`p-1 m-1 ${style.card_close}`}
        onClick={() => handleDelete(id)}
      >
        <IconSquareX color='red' />
      </div>
      <div
        className='card overflow-hidden'
        onClick={() => router.push(`/blog/${id}`)}
      >
        <div className={`row ${style.card}`}>
          <div className='col-lg-5 col-md-6 col-12 pe-lg-0'>
            <a href='javascript:;'>
              <div className='p-2'>
                <img
                  className={`card_image w-100 border-radius-md rounded ${style.card_image}`}
                  src={image}
                  alt='image'
                />
              </div>
            </a>
          </div>
          <div className='col-lg-7 col-md-6 col-12 ps-lg-0 my-2'>
            <div className={`card-body p-1 ${style.card_body}`}>
              <ChipDate createAt={creatAt} />
              <h5 className='my-2 pr-2'>{title}</h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Card
