import React from 'react'
import { format } from 'date-fns'
import style from './card.module.scss'

function ChipDate({ createAt }: { createAt: string }) {
  return (
    <span className={`px-2 py-1 ${style.card_chip}`}>
      {format(new Date(createAt), 'hh:mm  dd/MM/yyyy')}
    </span>
  )
}

export default ChipDate
