import React from 'react'
import Markdown from 'react-markdown'
import rehypeRaw from 'rehype-raw'
import remarkGfm from 'remark-gfm'
import remarkToc from 'remark-toc'
import rehypeSanitize from 'rehype-sanitize'
function MarkDownPreview({ content }: { content: string }) {
  return (
    <p className='text-lg mb-0'>
      <Markdown
        rehypePlugins={[rehypeRaw, rehypeSanitize, remarkGfm, remarkToc]}
      >
        {content}
      </Markdown>
    </p>
  )
}

export default MarkDownPreview
