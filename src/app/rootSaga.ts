import { all } from 'redux-saga/effects'
import { blogSaga } from 'features/blog/blogSaga'

export default function* rootSaga() {
  // saga
  yield all([blogSaga()])
}
