import { combineReducers } from 'redux'
import bogReducer from 'features/blog/blogSlice'

export default combineReducers({
  // slices
  blog: bogReducer,
})
