export const imageType = {
  'image/png': ['.png'],
  'image/jpeg': ['.jpeg'],
  'image/gif': ['.gif'],
}
