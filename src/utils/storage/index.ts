export const STORAGE_KEY = {
  JWT: 'JWT',
  USER: 'USER',
}

export default class StorageUtil {
  static get = (key: string) => {
    try {
      const setValue = localStorage.getItem(key) || ''
      return JSON.parse(setValue)
    } catch {
      return ''
    }
  }

  static set = (key: string, value: any) => {
    try {
      return localStorage.setItem(key, JSON.stringify(value))
    } catch {
      return ''
    }
  }

  static remove = (key: string) => {
    localStorage.removeItem(key)
  }
}
