import MarkDownPreview from 'ui-component/MarkDown/MarkDownPreview'
import ChipDate from 'ui-component/Card/Chip'
import Button from 'layout/Header/Button'
import { useRouter } from 'next/router'

interface IBlogDetail {
  id?: any
  title: string
  content: string
  image: string
  createAt?: string
}

function BlogDeatil({ id, title, content, image, createAt }: IBlogDetail) {
  const router = useRouter()
  return (
    <section className='py-sm-5 py-3 position-relative'>
      <div className='container'>
        <div className='row '>
          {id && title && (
            <div className='col-12 col-lg-3 col-md-4 position-relative'>
              <div onClick={() => router.push(`/blog/update/${id}`)}>
                <Button>EDIT BLOG</Button>
              </div>
            </div>
          )}
          <div className='col-12 mx-auto'>
            <div className='row py-lg-4 py-5 '>
              <div className='col-12 col-lg-3 col-md-4 position-relative'>
                <img
                  className='border-radius-lg max-width-200 w-100 position-relative z-index-2 rounded mx-auto d-block'
                  src={image}
                />
              </div>
              <div className='col-12 col-lg-9 col-md-8 z-index-2 position-relative px-md-2 px-sm-5 mt-sm-0 mt-4'>
                <div className='d-flex justify-content-between align-items-center mb-2'>
                  <h4 className='mb-0'>{title}</h4>
                </div>
                <div className='row mb-4'>
                  <div className='col-auto'>
                    {createAt && <ChipDate createAt={createAt} />}
                  </div>
                </div>
                <MarkDownPreview content={content} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default BlogDeatil
