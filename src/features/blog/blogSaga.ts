import { PayloadAction } from '@reduxjs/toolkit'
import { BlogAPI, IBlogDetail, IBlog, IBlogCreateForm } from 'api/blog'
import {
  IServerResponse,
  IServerDetailResponse,
  ICallApiDetail,
} from 'api/config'
import { AxiosResponse } from 'axios'
import Router from 'next/router'
import {
  all,
  call,
  CallEffect,
  put,
  PutEffect,
  takeLatest,
  select,
} from 'redux-saga/effects'
import { IQuery } from 'types/query'
import {
  // get list
  failgetListBlog,
  getListBlog,
  successgetListBlog,
  // get detail
  getDetailBlog,
  successGetDetailBlog,
  failGetDetailBlog,
  // create
  createBlog,
  successCreateBlog,
  failCreateBlog,
  // update
  updateBlog,
  failUpdateBlog,
  successUpdateBlog,
  // delete
  deleteBlog,
  failDeleteBlog,
  successDeleteBlog,
  selectQuery,
} from './blogSlice'

function* _getListBlog({
  payload,
}: PayloadAction<IQuery>): Generator<CallEffect | PutEffect> {
  try {
    const response = (yield call(
      BlogAPI.getListBlog,
      payload
    )) as AxiosResponse<IServerResponse<IBlog>>
    const { pagination, data } = response.data
    yield put(successgetListBlog({ result: data.items, paginate: pagination }))
  } catch {
    yield put(failgetListBlog())
  }
}

function* _getDetailBlog({
  payload,
}: PayloadAction<number>): Generator<CallEffect | PutEffect> {
  try {
    const response = (yield call(
      BlogAPI.getDetailBlog,
      payload
    )) as AxiosResponse<IServerDetailResponse<IBlogDetail>>
    const { data } = response.data
    yield put(successGetDetailBlog(data))
  } catch {
    yield put(failGetDetailBlog())
  }
}

function* _createBlog({
  payload,
}: PayloadAction<IBlogCreateForm>): Generator<CallEffect | PutEffect> {
  try {
    const formData = new FormData()
    formData.append('blog[title]', payload.title)
    formData.append('blog[content]', payload.content)
    formData.append('blog[image]', payload.image)
    const res = (yield call(
      BlogAPI.createBlog,
      formData
    )) as ICallApiDetail<IBlogDetail>
    const { data } = res.data
    yield put(successCreateBlog(data))
    yield call(Router.push, `/blog/${res.data.data.id}`)
  } catch {
    yield put(failCreateBlog())
  }
}

function* _updateBlog({
  payload,
}: PayloadAction<any>): Generator<CallEffect | PutEffect> {
  try {
    const formData = new FormData()
    formData.append('blog[title]', payload.title)
    formData.append('blog[content]', payload.content)
    payload.image && formData.append('blog[image]', payload.image)
    yield call(BlogAPI.updateBlog, { id: payload.id, formData })
    yield put(successUpdateBlog())
    yield call(Router.push, `/blog/${payload.id}`)
  } catch {
    yield put(failUpdateBlog())
  }
}

function* _deleteBlog({ payload }: PayloadAction<number>): Generator {
  try {
    yield call(BlogAPI.deleteBlog, payload)
    yield put(successDeleteBlog())
    const query = yield select(selectQuery)
    yield put(getListBlog(query as IQuery))
  } catch (error) {
    yield put(failDeleteBlog())
  }
}

export function* blogSaga() {
  yield all([
    takeLatest(getListBlog, _getListBlog),
    takeLatest(getDetailBlog, _getDetailBlog),
    takeLatest(createBlog, _createBlog),
    takeLatest(updateBlog, _updateBlog),
    takeLatest(deleteBlog, _deleteBlog),
  ])
}
