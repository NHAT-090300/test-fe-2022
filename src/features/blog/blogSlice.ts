import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IBlog, IBlogDetail, IBlogCreateForm } from 'api/blog'
import { RootState } from 'app/store'
import { IPaginate } from 'types/paginate'
import { IQuery } from 'types/query'

interface IList<T> {
  result: T[]
  paginate: IPaginate
}

const INIT_LIST = {
  result: [],
  paginate: {
    page: 1,
  },
}

interface IRequestStatus {
  processing: boolean
  fail: boolean
  success: boolean
}

const INIT_REQUEST_STATUS = {
  processing: false,
  fail: false,
  success: false,
}

const INIT_QUERY = {}

export interface IBlogState {
  createBlog: {
    status: IRequestStatus
    detail: IBlogDetail | Record<string, never>
  }
  updateBlog: {
    status: IRequestStatus
  }
  getListBlog: {
    list: IList<IBlog>
    status: IRequestStatus
    query: IQuery
  }
  getDetailBlog: {
    detail: IBlogDetail | Record<string, never>
    status: IRequestStatus
  }
  deleteBlog: {
    status: IRequestStatus
  }
}

export const initialState: IBlogState = {
  createBlog: {
    status: { ...INIT_REQUEST_STATUS },
    detail: {},
  },
  getListBlog: {
    list: { ...INIT_LIST },
    status: { ...INIT_REQUEST_STATUS },
    query: { ...INIT_QUERY },
  },
  getDetailBlog: {
    detail: {},
    status: { ...INIT_REQUEST_STATUS },
  },
  updateBlog: {
    status: { ...INIT_REQUEST_STATUS },
  },
  deleteBlog: {
    status: { ...INIT_REQUEST_STATUS },
  },
}

export const blogSlice = createSlice({
  name: '@blog',
  initialState,
  reducers: {
    createBlog: (state, action: PayloadAction<IBlogCreateForm>) => {
      state.createBlog.status.processing = !!action.payload
      state.createBlog.status.fail = false
      state.createBlog.status.success = false
    },
    stopCreateBlog: (state) => {
      state.createBlog.status.processing = false
    },
    failCreateBlog: (state) => {
      state.createBlog.status.fail = true
      state.createBlog.status.processing = false
    },
    successCreateBlog: (state, action: PayloadAction<IBlogDetail>) => {
      state.createBlog.status.success = true
      state.createBlog.status.processing = false
      state.createBlog.detail = action.payload
    },

    updateBlog: (state, action: PayloadAction<IBlogCreateForm>) => {
      state.updateBlog.status.processing = !!action.payload
      state.updateBlog.status.fail = false
      state.updateBlog.status.success = false
    },
    stopUpdateBlog: (state) => {
      state.updateBlog.status.processing = false
    },
    failUpdateBlog: (state) => {
      state.updateBlog.status.fail = true
      state.updateBlog.status.processing = false
    },
    successUpdateBlog: (state) => {
      state.updateBlog.status.success = true
      state.updateBlog.status.processing = false
    },

    getListBlog: (state, action: PayloadAction<IQuery>) => {
      state.getListBlog.status.success = false
      state.getListBlog.status.fail = false
      state.getListBlog.query = action.payload
      state.getListBlog.status.processing = !!action.payload
    },
    successgetListBlog: (state, action: PayloadAction<IList<IBlog>>) => {
      state.getListBlog.list = action.payload
      state.getListBlog.status.success = true
      state.getListBlog.status.processing = false
    },
    failgetListBlog: (state) => {
      state.getListBlog.list = { ...INIT_LIST }
      state.getListBlog.status.fail = true
      state.getListBlog.status.processing = false
    },

    getDetailBlog: (state, action: PayloadAction<number>) => {
      state.getDetailBlog.detail = {}
      state.getDetailBlog.status.success = false
      state.getDetailBlog.status.fail = false
      state.getDetailBlog.status.processing = !!action.payload
    },
    successGetDetailBlog: (state, action: PayloadAction<IBlogDetail>) => {
      state.getDetailBlog.detail = action.payload
      state.getDetailBlog.status.success = true
      state.getDetailBlog.status.processing = false
    },
    failGetDetailBlog: (state) => {
      state.getDetailBlog.status.fail = true
      state.getDetailBlog.status.processing = false
    },

    deleteBlog: (state, action: PayloadAction<number>) => {
      state.deleteBlog.status.processing = !!action.payload
      state.deleteBlog.status.fail = false
      state.deleteBlog.status.success = false
    },
    stopDeleteBlog: (state) => {
      state.deleteBlog.status.processing = false
    },
    failDeleteBlog: (state) => {
      state.deleteBlog.status.fail = true
    },
    successDeleteBlog: (state) => {
      state.deleteBlog.status.success = true
    },
  },
})

export const {
  // create
  createBlog,
  failCreateBlog,
  successCreateBlog,
  // update
  updateBlog,
  stopUpdateBlog,
  failUpdateBlog,
  successUpdateBlog,
  // get list
  getListBlog,
  successgetListBlog,
  failgetListBlog,
  //get detail
  getDetailBlog,
  successGetDetailBlog,
  failGetDetailBlog,
  // delete
  deleteBlog,
  stopDeleteBlog,
  failDeleteBlog,
  successDeleteBlog,
} = blogSlice.actions

export const selectQuery = (state: RootState) => state.blog.getListBlog.query

export default blogSlice.reducer
