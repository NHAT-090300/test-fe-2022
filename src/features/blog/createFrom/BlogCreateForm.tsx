// tsrsfc
import React from 'react'
import { FormikProps, useFormik } from 'formik'
import TextField from 'ui-component/Feild/textFeild'
import Dropzone from 'ui-component/Dropzone'
import blogValidationSchema from 'validate/blog'
import { IBlog, IBlogCreateForm } from 'api/blog'

interface TextField {
  title?: string
  content?: string
  TextField?: React.ReactNode
  handleSubmit?: () => void
}

const initialValues = {
  title: '',
  content: '',
  image: '',
}

export const AppTextFeild = ({
  label,
  name,
  type = '',
  formik,
}: {
  label: string
  name: string
  type?: string
  formik: any
  errors?: boolean
}) => {
  return (
    <TextField
      label={label}
      name={name}
      type={type}
      handleChange={(value: string) => formik.setFieldValue(name, value)}
      value={formik.values[name]}
      error={formik.errors[name]}
    />
  )
}

const convertValue = (blog: IBlog) => {
  const { id, title, content, image } = blog
  return {
    id,
    title,
    content,
    image: image?.url,
  }
}

const BlogCreateForm = ({
  dispatch,
  initValue,
}: {
  dispatch: any
  initValue?: IBlog
}) => {
  const formik: FormikProps<IBlogCreateForm> = useFormik({
    enableReinitialize: true,
    validateOnChange: false,
    initialValues: initValue ? convertValue(initValue) : initialValues,
    validationSchema: blogValidationSchema,
    onSubmit: (values: IBlogCreateForm) => {
      const { id, title, content, image } = values
      let body
      if (initValue) {
        body =
          typeof values.image == 'string'
            ? {
                id,
                title,
                content,
              }
            : {
                id,
                title,
                content,
                image,
              }
      } else {
        body = {
          title,
          content,
          image,
        }
      }
      dispatch({ ...body })
    },
  })

  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <div className='row'>
          <div className='col-lg-4 col-12 mx-auto'>
            <Dropzone
              onFileUploaded={(file: any) => {
                formik.setFieldValue('image', file)
              }}
              formik={formik}
              value={formik.values['image']}
              name='image'
            />
          </div>
          <div className='col-lg-8 col-12 my-5'>
            <AppTextFeild
              label='Title'
              name='title'
              type='text'
              formik={formik}
            />
            <AppTextFeild label='Content' name='content' formik={formik} />
          </div>
          <div
            style={{ border: 'none' }}
            className='col-12 d-flex justify-content-end'
          >
            <button
              className='btn btn-dark my-5 '
              type='submit'
              onClick={() => formik.submitForm}
            >
              <div className='px-2 text-center'>Submit</div>
            </button>
          </div>
        </div>
      </form>
    </>
  )
}

export default BlogCreateForm
