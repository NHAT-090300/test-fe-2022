import * as yup from 'yup'

const blogValidationSchema = yup.object({
  title: yup
    .string()
    .max(255, 'The title should not exceed 255')
    .trim()
    .required('The title is required'),
  content: yup
    .string()
    .nullable()
    .trim()
    .max(10000, 'The name should not exceed 10000 characters')
    .required('The image is required'),
  image: yup.string().required('The image is required'),
})

export default blogValidationSchema
