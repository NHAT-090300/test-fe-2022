export { API_BLOG, default as BlogAPI } from './api'

export type { default as IBlog, IBlogDetail, IBlogCreateForm } from './type'
