export default interface IBlog {
  id: number
  title: string
  content: string
  image: {
    url: string
  }
  created_at: string
  updated_at: string
}

export type IBlogDetail = Omit<IBlog, 'created_at'> & {
  comments_count: number
}

export type IBlogCreateForm = {
  id?: number
  title: string
  content: string
  image?: any
}
