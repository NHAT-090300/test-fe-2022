import { IQuery } from 'types/query'
import { appAxios, currentVersion } from '../config'
import { IBlog } from './index'

export const API_BLOG = {
  GET_LIST_BLOG: `${currentVersion}/blogs`,
  GET_DETAIL_BLOG: (id: number) => `${currentVersion}/blogs/${id}`,
  CREATE_BLOG: `${currentVersion}/blogs`,
  UPDATE_BLOG: (id: number) => `${currentVersion}/blogs/${id}`,
  DELETE_BLOG: (id: number) => `${currentVersion}/blogs/${id}`,
}

export default class BlogAPI {
  static getListBlog = (params?: IQuery) =>
    appAxios.get(API_BLOG.GET_LIST_BLOG, params)
  static getDetailBlog = (id: number) =>
    appAxios.get<IBlog>(API_BLOG.GET_DETAIL_BLOG(id))
  static createBlog = (params?: any) =>
    appAxios.post(API_BLOG.CREATE_BLOG, params)
  static updateBlog = (params?: any) =>
    appAxios.put(API_BLOG.UPDATE_BLOG(params.id), params.formData)
  static deleteBlog = (id: number) => appAxios.delete(API_BLOG.DELETE_BLOG(id))
}
