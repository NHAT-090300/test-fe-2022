export { default as appAxios } from './axios-config'
export { currentVersion } from './version'
export type {
  default as IServerResponse,
  IServerResponseWithoutPaginate,
  ICallApiDetail,
  ICatchError,
  IServerDetailResponse,
} from './type'
