import { IPaginate } from 'types/paginate'
import { AxiosError, AxiosResponse } from 'axios'

interface IServerResponse<T> {
  code: number
  data: {
    items: T[]
  }
  pagination: IPaginate
}

export type IServerResponseWithoutPaginate<T> = Pick<
  IServerResponse<T>,
  'code'
> & {
  data: T[]
}

export type IServerDetailResponse<T> = {
  code: number
  data: T
}

export type ICallApiDetail<T> = AxiosResponse<IServerDetailResponse<T>>

export type ICatchError<T> = AxiosError<T>

export default IServerResponse
